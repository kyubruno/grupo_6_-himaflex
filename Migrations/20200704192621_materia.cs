﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Himaflex.Migrations
{
    public partial class materia : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MateriaPrimaId",
                table: "Produtos",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_MateriaPrimaId",
                table: "Produtos",
                column: "MateriaPrimaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Produtos_MateriaPrimas_MateriaPrimaId",
                table: "Produtos",
                column: "MateriaPrimaId",
                principalTable: "MateriaPrimas",
                principalColumn: "MateriaPrimaId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Produtos_MateriaPrimas_MateriaPrimaId",
                table: "Produtos");

            migrationBuilder.DropIndex(
                name: "IX_Produtos_MateriaPrimaId",
                table: "Produtos");

            migrationBuilder.DropColumn(
                name: "MateriaPrimaId",
                table: "Produtos");
        }
    }
}
