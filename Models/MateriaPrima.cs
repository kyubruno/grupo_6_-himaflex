﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Himaflex.Models
{
    public class MateriaPrima
    {
        public int MateriaPrimaId { get; set; }
        public string Nome { get; set; }

        [DisplayName("Quantidade (KG)")]
        public int Quantidade { get; set; }

        public ICollection<Produto> produtos { get; set; }

    }
}
