﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Himaflex.Models
{
    public class Produto
    {
       
        public int ProdutoId { get; set; }


        [DisplayName("Nome do Produto")]
        public string Nomeproduto { get; set; }


        public string Descricao { get; set; }

        public string Bitola { get; set; }


        [DisplayName("Pressão (lbs/pol)")]
        public int Pressao { get; set; }



        [DisplayName("Vácuo (pol.hg)")]
        public int  Vacuo { get; set; }



        [DisplayName("Raio Curv. (mm)")]
        public int Raio { get; set; }



        [DisplayName("Parede (m/m)")]
        public float  Parede { get; set; }



        [DisplayName("Ø Interno (m/m)")]
        public float Interno { get; set; }



        [DisplayName("Ø Externo (m/m)")]
        public float Externo { get; set; }



        [DisplayName("Cor")]
        public string Cor { get; set; }


        [DisplayName("Categoria")]
        public int CategoriaId { get; set; }
        public Categoria categoria { get; set; }



        public MateriaPrima materiaPrima { get; set; }
        [DisplayName("Materia Prima")]
        public int MateriaPrimaId { get; set; }


       public ICollection<Pedido> pedidos { get; set; }

    }
}
