﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Himaflex.Models
{
    public class Categoria
    {
        
        public int CategoriaId { get; set; }

        [Required(ErrorMessage = "A descrição precisa ser preenchida")]
        [DisplayName("Descrição")]
        public string Descricao { get; set; }

        public ICollection<Produto> produtos { get; set; }
        public ICollection<Pedido> pedidos { get; set; }
    
    }
}
