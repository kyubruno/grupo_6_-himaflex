﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Himaflex.Models
{
    public class InicializaBD
    {
        public static void Initialize(Context context)
        {
            context.Database.EnsureCreated();

            if (context.Categorias.Any())
            {
                return;
            }

            var categorias = new Categoria[]
            {
                new Categoria{Descricao="Linha ATÓXICA"},
                new Categoria{Descricao="Linha CASA E JARDIM"},
                new Categoria{Descricao="Linha INDUSTRIAL"},
                new Categoria{Descricao="Linha SUCÇAO E DESCARGA"},
                new Categoria{Descricao="Linha USO GERAL"},
                new Categoria{Descricao="Linha VÁCUO"},
            };

            foreach (var item in categorias)
            {
                context.Categorias.Add(item);
            }

            context.SaveChanges();




            var produtos = new Produto[]
            {
            };

            foreach (var item in produtos)
            {
                context.Produtos.Add(item);
            }
            context.SaveChanges();



            var pedidos = new Pedido[]
            {
            };

            foreach (var item in pedidos)
            {
                context.Pedidos.Add(item);
            }
            context.SaveChanges();




        }
    }
}
