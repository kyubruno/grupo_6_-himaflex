﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Himaflex.Models
{
    public class Pedido
    {
        public DateTime Data { get; set;}
        
        public int PedidoId { get; set; }

        [DisplayName("Metragem")]
        public int metragem { get; set; }

        [DisplayName("Gravação na mangueira")]
        public string gravacao { get; set; }

        [DisplayName("Categoria")]
        public int CategoriaId { get; set; }
        public Categoria categoria { get; set; }


        public Produto produto { get; set; }

        [DisplayName("Observação")]
        public string Nomeproduto { get; set; }

        [DisplayName("Produto")]
        public int ProdutoId { get; set; }
        



    }
}
